//Tiers can be forced by the programmer using the syntax "type<tier> var;", e.g. "int<0> x;".

class Exe {
    int max(int a, int b) {
        int<0> res = b;
        if(a > b) {
            res = a;
        }
        return res;
    }

    int add(int x, int y) {
        while(x > 0) {
            x = x - 1;
            y = y + 1;
        }
        return y;
    }

    void main() {

    }
}