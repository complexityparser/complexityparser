//Tiers can be forced by the programmer using the syntax "type<tier> var;", e.g. "int<0> x;".

class Exe {
    int max(int a, int b) {
        int<0> res = b;
        if(a > b) {
            res = a;
        }
        return res;
    }
    int add(int x, int y) {
        while(x > 0) {
            x = x - 1;
            y = y + 1;
        }
        return y;
    }

    int mult(int x, int y) {
        int<0> z = 0;

        while(x > 0) {
            x = x - 1;

            z = add(y, z);
        }

        return z;
    }

    int exp(int x) {
        int<0> res = 1;

        while(x > 0) {
            res = res + res;
            x = x - 1;
        }
        return res;
    }

    int cube(int x) {
        int<1> y = x;
        int<0> res = 0;

        while(x > 0) {
            int<1> u = y;
            x = x - 1;
            while(u > 0) {
                u = u - 1;

                res = add(y, res);
            }
        }

        return res;
    }

    int sum(int x) {
        int res = 0;

        while (x > 0) {
            res = add(x, res);
            x = x - 1;
        }

        return res;
    }

    int fibo(int x) {
        int<0> res = 1;
        int<0> a = 1;
        int<0> b = 1;

        while(x > 0) {
            res = a;
            a = a + b;
            b = res;
            x = x - 1;
        }

        return res;
    }



    void main() {
        #init
            Exe e1 = new Exe();
        #init

        Exe e0 = new Exe();

        e1.sum(10);
        e0.sum(10);
    }
}