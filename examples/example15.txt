//Tiers can be forced by the programmer using the syntax "type<tier> var;", e.g. "int<0> x;".

class List {
    int data;
    List next;

    public List(int data, List next) {
        this.data = data;
        this.next = next;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public List getNext() {
        return next;
    }

    public void setNext(List next) {
        this.next = next;
    }

    public int mult(int x, int y) {
        int<0> z = 0;

        while(x > 0) {
            x = x - 1;

            z = z + y;
        }

        return z;
    }

    public int sum() {return data;}
}

class UList extends List {
    public int sum() {
        int res = data;
        if(next != null) {
            res = res + mult(1000, next.sum());
        }
        return res;
    }
}

class UselessList extends List {
    public int length() {
        while(true) {}
        return 1;
    }

    public int sum() {
        return 1;
    }
}

class Exe {
    void main() {
        #init
            UList ul1 = new UList(10, null);
            ul1 = new UList(11, ul1);

            List l1 = new List(10, null);
            l1 = new List(11, null);
        #init

        List<1> l = ul1.getNext();
        if(l.getData() > 0) {
            while (true) {}
        }
        //the sum block types in 1, and it's result is 0 thus it cannot be used inside a conditional.
	//This would break the control flow discipline.
        if(l.sum() > 0) {}
    }
}