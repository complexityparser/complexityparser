/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import complexityparser.Model;
import complexityparser.types.env.TypingEnv;
import view.MainFrame;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            TypingEnv.loadInstanceFromFile();
        } catch (IOException e) {
            System.err.println("Operator config file load failed, exiting");
            System.exit(1);
        }
        Model m = new Model();
        MainFrame frame = new MainFrame(m);
    }
}