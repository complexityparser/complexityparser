/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package view;

import complexityparser.Model;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.awt.event.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * The main window.
 */
public class MainFrame extends JFrame {

    public MainFrame(Model m) {
        setTheme();
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("ComplexityParser");
        BorderLayout layout = new BorderLayout();
        setLayout(layout);
        add(new EditorView(m, this), BorderLayout.WEST);
        add(new TreePanel(m), BorderLayout.CENTER);
        add(new MessageView(m), BorderLayout.EAST);	
        setJMenuBar(new Menu(m, this));
        pack();
        //setSize(600,600);
    }

    /**
     * Changes the look and feel of java swing to the OS theme.
     */
    private static void setTheme() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }
}
