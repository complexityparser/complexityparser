/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package view;

import complexityparser.Model;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * A basic view to display a text editor containing the code that is analysed
 * and displayed by the ANTLR tree.
 */
public class Menu extends JMenuBar {

    private Model m;
    private MainFrame parent;
    private JTextArea area;

    public Menu(Model m, MainFrame parent) {
        this.parent = parent;
        this.m = m;
        area = new JTextArea(m.getCode());
        JMenu menu = new JMenu("File");
        JMenuItem open = new JMenuItem("Open (CTRL+o)");
        menu.add(open);
        open.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fc = new JFileChooser();
                String s = System.getProperty("user.dir") + System.getProperty("file.separator") + "examples";
                fc.setCurrentDirectory(new File(s));
                if (JFileChooser.APPROVE_OPTION == fc.showOpenDialog(parent)) {
                    File f = fc.getSelectedFile();
                    try {
                        m.setSyntaxError(false);
                        byte[] res = Files.readAllBytes(f.toPath());
                        String txt = new String(res, StandardCharsets.UTF_8);
                        area.setText(txt);
                        m.setCode(txt);
                        parent.setTitle(f.toString());
                        m.update();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IndexOutOfBoundsException e) {
                        area.setText(area.getText());
                        m.setSyntaxError(true);
                        m.update();
                    }
                }
            }
        });
        JMenuItem exit = new JMenuItem("Exit ComplexityParser");
        menu.add(exit);
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        add(menu);
    }
}
