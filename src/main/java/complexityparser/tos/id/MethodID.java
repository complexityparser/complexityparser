/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.tos.id;

import java.util.ArrayList;
import java.util.Objects;

public class MethodID extends ID {

    ArrayList<String> param;

    public MethodID(String name, ArrayList<String> p) {
        super(name);
        param = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        MethodID methodID = (MethodID) o;
        return Objects.equals(param, methodID.param);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), param);
    }

    @Override
    public String getName() {
        StringBuilder str = new StringBuilder();

        str.append(super.getName());
        str.append("(");

        for (int i = 0; i < param.size(); i++) {
            str.append(param.get(i));

            if (i < param.size() - 1) {
                str.append(", ");
            }
        }
        str.append(")");
        return str.toString();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        StringBuilder param = new StringBuilder(this.param.toString());
        param.setCharAt(0, '(');
        param.setCharAt(param.length() - 1, ')');
        str.append("MethodID { name ='");
        str.append(getName());
        str.append("' param = '");
        str.append(param);
        str.append("' }");
        return str.toString();
    }
}
