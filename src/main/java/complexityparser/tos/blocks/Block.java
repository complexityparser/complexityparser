/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.tos.blocks;

import complexityparser.tos.TOS;
import complexityparser.tos.id.ID;
import complexityparser.types.tieredtypes.MethodTieredType;
import complexityparser.types.tieredtypes.TieredType;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A class representing a block and containing: - the name of the block, - the
 * index no of the block, - the parent index noParent of the block, - a hashmap
 * symbols containing every (ID, TieredType) entry (ID has to be unique). This
 * class should not be instanciated outside of the TOS class.
 */
public class Block {

    private String name;
    private int no;
    private int noParent;
    protected HashMap<ID, TieredType> symbols;

    public Block(String name) {
        this.name = name;
        noParent = TOS.getInstance().getCurrentBlock();
        no = TOS.getInstance().getNextBlockNumber();
        symbols = new HashMap<>();
    }

    public Block(String name, int no, int noParent) {
        this.name = name;
        this.no = no;
        this.noParent = noParent;
        symbols = new HashMap<>();
    }

    /**
     * Adds an entry to a block.
     *
     * @param i - an ID.
     * @param t - a tiered type.
     */
    public void put(ID i, TieredType t) {
        symbols.put(i, t);
    }

    /**
     *
     * @param i - an ID.
     * @return the associated tiered type of the given ID (if the ID exists).
     */
    public TieredType get(ID i) {
        return symbols.get(i);
    }

    /**
     *
     * @return the index of the parent block.
     */
    public int getNoParent() {
        return noParent;
    }

    /**
     *
     * @return the name of the current block.
     */
    public String getName() {
        return name;
    }

    public String getBlockName() {
        return getName();
    }

    /**
     *
     * @return the name of the parent class (makes no sense on anonym blocks or
     * methods).
     */
    public String getMother() {
        return "Object";
    }

    /**
     *
     * @return the index of the current block.
     */
    public int getNo() {
        return no;
    }

    public MethodTieredType findMethodTieredTypeByNamespace(String name) {
        for (Map.Entry<ID, TieredType> e : symbols.entrySet()) {
            if (e.getKey().getName().equals(name)) {
                return (MethodTieredType) e.getValue();
            }
        }
        return null;
    }

    /**
     *
     * @return a set containing the (ID, TieredType) entries.
     */
    public Set<Map.Entry<ID, TieredType>> getMap() {
        return symbols.entrySet();
    }
}
