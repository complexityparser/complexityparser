/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.listeners;

import complexityparser.types.Tier;
import complexityparser.analyse.antlr.JavaParser;
import complexityparser.analyse.antlr.JavaParserBaseListener;
import complexityparser.tos.TOS;
import complexityparser.tos.id.ClassID;
import complexityparser.tos.id.MethodID;
import complexityparser.tos.id.VariableID;
import complexityparser.types.tieredtypes.ClassTieredType;
import complexityparser.types.tieredtypes.MethodTieredType;
import complexityparser.types.tieredtypes.VariableTieredType;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * A basic listener (visitor) exploring the parse tree and filling the TOS with
 * the appropriate symbol id and tiered type.
 */
public class TOSBuilder extends JavaParserBaseListener {

    private TOS tos;
    private ParseTreeProperty<Integer> blockNumbers;
    private JavaParser.BlockContext lastEntered;
    private String blockName;
    private ArrayList<String> methodParams;
    private Tier defaultTier;

    public TOSBuilder() {
        tos = TOS.getInstance();
        tos.clear();
        blockNumbers = new ParseTreeProperty<>();
        blockName = "<>";
        methodParams = null;
        defaultTier = Tier.T0;
    }

    /**
     *
     * @return a map from parse tree to Integer reprenting the block number
     * associated with each parse tree node.
     */
    public ParseTreeProperty<Integer> getBlockNumbers() {
        return blockNumbers;
    }

    /**
     * Handles class field declarations.
     *
     * @param ctx - a field declaration context.
     */
    @Override
    public void enterFieldDeclaration(JavaParser.FieldDeclarationContext ctx) {
        super.enterFieldDeclaration(ctx);
        VariableTieredType t = new VariableTieredType(Tier.T0, ctx.typeType().getText());
        List<JavaParser.VariableDeclaratorContext> list = ctx.variableDeclarators().variableDeclarator();
        /*
        A field declaration may contain multiple fields, e.g. private int a, b, c;
         */
        for (int i = 0; i < list.size(); i++) {
            VariableID id = new VariableID(list.get(i).variableDeclaratorId().getText());
            tos.put(id, t);
        }
    }

    /**
     * Handles a block (class blocks are treated differently).
     *
     * @param ctx - a block context.
     */
    @Override
    public void enterBlock(JavaParser.BlockContext ctx) {
        super.enterBlock(ctx);
        if (ctx.equals(lastEntered)) {
            lastEntered = null;
        } else {
            tos.enterBlock("<>");
        }
    }

    /**
     * Exits a block (class blocks are treated differently).
     *
     * @param ctx - a block context.
     */
    @Override
    public void exitBlock(JavaParser.BlockContext ctx) {
        super.exitBlock(ctx);
        tos.exitBlock();
    }

    /**
     * Sets the default tier to T1 when entering a #init #init block.
     *
     * @param ctx - a statement context.
     */
    @Override
    public void enterStatement_init(JavaParser.Statement_initContext ctx) {
        super.enterStatement_init(ctx);
        defaultTier = Tier.T1;
    }

    /**
     * Sets the default tier to 0 when exiting a #init #init block.
     *
     * @param ctx - a statement context.
     */
    @Override
    public void exitStatement_init(JavaParser.Statement_initContext ctx) {
        super.exitStatement_init(ctx);
        defaultTier = Tier.T0;
    }

    /**
     * Associates a block number to an expression call in the block numbers map
     * (mapping ParseTree to Integer).
     *
     * @param ctx - an expression call context.
     */
    @Override
    public void enterExpression_call(JavaParser.Expression_callContext ctx) {
        super.enterExpression_call(ctx);
        blockNumbers.put(ctx, tos.getCurrentBlock());
    }

    /**
     * Handles class declarations.
     *
     * @param ctx - a class declaration context.
     */
    @Override
    public void enterClassDeclaration(JavaParser.ClassDeclarationContext ctx) {
        super.enterClassDeclaration(ctx);
        String mother = "Object";
        if (ctx.typeType() != null) {
            mother = ctx.typeType().getText();
        }
        String name = ctx.IDENTIFIER().getText();
        tos.put(new ClassID(name), new ClassTieredType(Tier.None, mother, name));
        tos.enterClass(name, mother);
        blockNumbers.put(ctx, tos.getCurrentBlock());
    }

    /**
     * Associates a block number to a method call context in the block numbers
     * map (mapping ParseTree to Integer).
     *
     * @param ctx - a method call context.
     */
    @Override
    public void enterMethodCall(JavaParser.MethodCallContext ctx) {
        super.enterMethodCall(ctx);
        blockNumbers.put(ctx, tos.getCurrentBlock());
    }

    /**
     * Handles constructor declarations.
     *
     * @param ctx - a constructor declaration context.
     */
    @Override
    public void enterConstructorDeclaration(JavaParser.ConstructorDeclarationContext ctx) {
        super.enterConstructorDeclaration(ctx);
        lastEntered = ctx.block();
        String name = ctx.IDENTIFIER().getText();
        ArrayList<String> param = new ArrayList<>();
        ArrayList<String> pNames = new ArrayList<>();
        /*
        Checks for constructor parameters and populate the param list with a string representation of their type
        to create the constructors namespace (that allows us to identify uniquely every constructor).
         */
        if (ctx.formalParameters().formalParameterList() != null) {
            for (JavaParser.FormalParameterContext t : ctx.formalParameters().formalParameterList().formalParameter()) {
                String pName = t.variableDeclaratorId().IDENTIFIER().getText();
                String pType = t.typeType().getText();
                param.add(pType);
                pNames.add(pName);
            }
        }
        /*
  	The following code associates a block number to the constructor in the block numbers map. 
         */
        blockNumbers.put(ctx, tos.getCurrentBlock());
        /*
        A method id/tiered type pair is associated to the constructor (constructors are treated as methods).
         */
        tos.put(new MethodID(name, param), new MethodTieredType(Tier.T0, name));
        tos.enterMethod(name, param);
        /*
        For each parameter, a variable id/tiered type pair is created and added in the tos.
         */
        for (int i = 0; i < pNames.size(); i++) {
            VariableID id = new VariableID(pNames.get(i));
            VariableTieredType t = new VariableTieredType(Tier.T0, param.get(i));

            tos.put(id, t);
        }
    }

    /**
     * Handles method declarations.
     * 
     * @param ctx - a method declaration context.
     */
    @Override
    public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        super.enterMethodDeclaration(ctx);
        ArrayList<String> params = new ArrayList<>();
        ArrayList<String> pNames = new ArrayList<>();
        String name = ctx.IDENTIFIER().getText();
        /*
        Checks for method parameters and their corresponding type.
         */
        if (ctx.formalParameters().formalParameterList() != null) {
            List<JavaParser.FormalParameterContext> list = ctx.formalParameters().formalParameterList().formalParameter();
            for (JavaParser.FormalParameterContext p : list) {
                String pName = p.variableDeclaratorId().IDENTIFIER().getText();
                params.add(p.typeType().getText());
                pNames.add(pName);
            }
        }
        /*
        Creates the method id and tiered type and adds them to the tos.
         */
        MethodID id = new MethodID(name, params);
        MethodTieredType t = new MethodTieredType(Tier.T0, ctx.typeTypeOrVoid().getText(), ctx);
        blockNumbers.put(ctx, tos.getCurrentBlock());
        tos.put(id, t);
        tos.enterMethod(name, params);

        lastEntered = ctx.methodBody().block();

        /*
        Creates id/tiered types for the method parameters and adds them to the tos.
         */
        for (int i = 0; i < pNames.size(); i++) {
            VariableID vid = new VariableID(pNames.get(i));
            VariableTieredType vt = new VariableTieredType(Tier.T0, params.get(i));
            tos.put(vid, vt);
        }
    }

    /**
     * Exits a class Block.
     *
     * @param ctx - a class body context.
     */
    @Override
    public void exitClassBody(JavaParser.ClassBodyContext ctx) {
        super.exitClassBody(ctx);
        tos.exitBlock();
    }

    /**
     * Handles a variable declaration context.
     *
     * @param ctx - a variable declaration context.
     */
    @Override
    public void enterLocalVariableDeclaration(JavaParser.LocalVariableDeclarationContext ctx) {
        super.enterLocalVariableDeclaration(ctx);
        Tier res;
        /*
        The tiered type of a local variable does not need to be specified.
        If the tier is not provided, then the current default value is used.
         */
        if (ctx.TIER() != null) {
            String tier = ctx.TIER().getText();
            tier = tier.substring(1, tier.length() - 1);
            res = Tier.toTier(Integer.valueOf(tier));
        } else {
            res = defaultTier;
        }
        VariableTieredType t = new VariableTieredType(res, ctx.typeType().getText());
        /*
        A local variable may contain multiple variables, e.g. int a, b = 10, c, d = 21;
         */
        for (JavaParser.VariableDeclaratorContext var : ctx.variableDeclarators().variableDeclarator()) {
            String name = var.variableDeclaratorId().getText();
            tos.put(new VariableID(name), t);
        }
        blockNumbers.put(ctx, tos.getCurrentBlock());
    }

    /**
     * Associates a block number to a primary expression (see the ANTLR grammar
     * for a definition of primary expressions) in the block numbers map.
     *
     * @param ctx - an expression primary context.
     */
    @Override
    public void enterExpression_primary(JavaParser.Expression_primaryContext ctx) {
        super.enterExpression_primary(ctx);
        blockNumbers.put(ctx, tos.getCurrentBlock());
    }

    /**
     * Associates a block number to a node in the block numbers map.
     *
     * @param ctx - a parser rule context.
     */
    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        super.enterEveryRule(ctx);
        blockNumbers.put(ctx, tos.getCurrentBlock());
    }

    @Override
    public void enterStatement_declass(JavaParser.Statement_declassContext ctx) {
        super.enterStatement_declass(ctx);
        blockNumbers.put(ctx, tos.getCurrentBlock());
    }
}