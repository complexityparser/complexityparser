/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.types.Tier;
import complexityparser.types.FOTierList;
import complexityparser.types.FOTier;
import complexityparser.types.TierArray;
import complexityparser.typingVisitors.base.ObjectVisitor;
import lib.graph.Graph;
import org.antlr.v4.runtime.CommonTokenStream;
import java.io.IOException;
import java.util.HashSet;

/**
 * The first pass of type inference. During this pass, all valid FOtiers are
 * built and entered in the typing environment (using the method
 * #visitMethodDeclaration). The incorrect FOtiers will be removed during the
 * second pass. The constructors FOtiers are only checked at the final pass
 * (there is no need to check them earlier especially if they contain method
 * calls that are valid now but may not be valid after the second pass).
 */
public class FirstPass extends ObjectVisitor {

    public FirstPass(Graph g, TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        super(g, tosBuilder, typingListener, stream);
    }

    /**
     * @param ctx - a method declaration context.
     * @return the tier of the method body.
     */
    @Override
    public Object visitMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        String namespace = tos.getMethodNamespace(ctx, getBlockNumber(ctx));
        Tier resEnv = Tier.None;
        Tier res = Tier.None;
        int nbParam = 0;
        if (ctx.formalParameters().formalParameterList() != null) {
            nbParam = ctx.formalParameters().formalParameterList().formalParameter().size();
        }
        boolean isPrimitiveReturnType = true;
        if (ctx.methodBody().block().expression() != null) {
            String type = getType(ctx.methodBody().block().expression());
            isPrimitiveReturnType = type.equals("int") || type.equals("boolean");
        }
        FOTierList op = new FOTierList(isPrimitiveReturnType);
        setOverrideResult(false);
        TierArray paramTiers = new TierArray(nbParam + 1);
        int noBloc = getBlockNumber(ctx.methodBody().block());
        /*
        Builds every possible FOTier types that be can given to the method parameters and checks if they are valid.
        If they are valid, they will be added to the typing environment.
        Recursive methods can be assigned to incorrect FOtiers in the first pass.
	This is the reason why a second pass has to be realized (that will loop until a fixpoint has been reached).
         */
        for (int i = 0; i < (1 << paramTiers.size()); i++) {
            setRecursiveCallsCount(0);
            setRecursiveCallsReceivers(new HashSet<>());
            setWhileCount(0);
            setActualEnvironment(paramTiers.get(0));
            if (nbParam != 0) {
                setParamTiers(ctx.formalParameters(), paramTiers, noBloc);
            }
            visitMethodBody(ctx.methodBody());
            Tier tmp = null;
            Tier env = null;
            if (ctx.methodBody().block().expression() != null) {
                tmp = getTier(ctx.methodBody().block().expression());
            }
            if (isRecursive()) {
                //safety conditions:
                if (getRecursiveCallsCount() == 1 && getWhileCount() == 0) {
                    env = Tier.T1;
                    if (tmp == null) {
                        tmp = Tier.T0;
                    }
                    try {
                        op.add(tmp, env, paramTiers.getArray());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    env = Tier.None;
                }
            } else {
                env = getTier(ctx.methodBody().block());
                if (ctx.THROWS() != null) {
                    env = Tier.max(Tier.T1, env);
                }
                if (tmp == null) {
                    tmp = env;
                }
                if (tmp != Tier.None && env != Tier.None) {
                    resEnv = env;
                    res = tmp;
                    try {
                        op.add(tmp, env, paramTiers.getArray());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    env = Tier.None;
                    tmp = Tier.None;
                }
            }
            paramTiers.increment();
        }
        te.add(namespace, op);
        setOverrideResult(true);
        return res;
    }
}
