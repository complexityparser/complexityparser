/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors.base;

import complexityparser.types.Tier;
import complexityparser.types.tieredtypes.TieredType;
import complexityparser.types.FOTier;
import complexityparser.types.FOTierList;
import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.tos.TOS;
import complexityparser.tos.id.VariableID;
import complexityparser.types.TierArray;
import lib.Mask;
import lib.graph.Graph;
import lib.graph.Node;
import org.antlr.v4.runtime.CommonTokenStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Base visitor extending BaseBlockStatementVisitor to handle basic objects.
 */
public abstract class ObjectVisitor extends BaseBlockStatementVisitor {

    private boolean isRecursive; //True if the current method is recursive.
    private boolean operatorModified; //A flag which is true if an entry in the operators/methods list has been modified during the pass.
    private ArrayList<ArrayList<Node>> components; //The strongly connected components in the call graph.

    public ObjectVisitor(Graph g, TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        this(tosBuilder, typingListener, stream);
        if (g != null) {
            components = g.getComponents();
            Collections.reverse(components);
        }
    }

    public ObjectVisitor(TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        super(tosBuilder, typingListener, stream);
        isRecursive = false;
        operatorModified = false;
    }

    /**
     *
     * @return true if the current method is recursive or mutually recursive.
     */
    public boolean isRecursive() {
        return isRecursive;
    }

    /**
     * Sets the isRecursive attribute to the given value.
     *
     * @param recursive - a boolean.
     */
    public void setRecursive(boolean recursive) {
        isRecursive = recursive;
    }

    /**
     *
     * @return true if an operator or method FOTier of the TypingEnv has been
     * modified/reduced.
     */
    public boolean isOperatorModified() {
        return operatorModified;
    }

    /**
     * Sets the #operatorModified attribute to the given value.
     *
     * @param operatorModified - a boolean.
     */
    public void setOperatorModified(boolean operatorModified) {
        this.operatorModified = operatorModified;
    }

    /**
     * Takes the strongly connected components in the call graph and verifies
     * them by calling the visitMethodDeclaration method on them. If a method is
     * recursive or mutually recursive, then the isRecursive attribute is set
     * accordingly.
     */
    public void visit() {
        for (ArrayList<Node> c : components) {
            clearRecursiveNamespaces();
            for (Node n : c) {
                addRecursiveNamespace(n.getName());
            }
            for (Node n : c) {
                setRecursive(c.size() != 1 || n.isRecursive());
                if (n.getCtx() != null) {
                    visit(n.getCtx());
                }
            }
        }
    }

    /**
     * A helper method setting the tiers of method parameters in the tos to the
     * given values.
     *
     * @param ctx - a formal parameters context.
     * @param paramTiers - an array of tiers.
     * @param noBloc - a block index.
     */
    protected void setParamTiers(JavaParser.FormalParametersContext ctx, TierArray paramTiers, int noBloc) {
        setParamTiers(ctx, paramTiers.getArray(), noBloc);
    }

    /**
     * A helper method setting the tiers of method parameters in the tos to the
     * given values.
     *
     * @param ctx - a formal tier parameters context.
     * @param paramTiers - an array of tiers.
     * @param noBloc - a block index.
     */
    protected void setParamTiers(JavaParser.FormalParametersContext ctx, Tier[] paramTiers, int noBloc) {
        int i = 1;
        for (JavaParser.FormalParameterContext c : ctx.formalParameterList().formalParameter()) {
            VariableID id = new VariableID(c.variableDeclaratorId().IDENTIFIER().getText());
            TieredType t = tos.find(id, noBloc);
            if (t != null) {
                t.setTier(paramTiers[i]);
            } else {
                System.err.println("TOSBuilder::setParamTiers error: " + id + " not found in TOS");
            }
            i++;
        }
    }

    /**
     * Handles a field of an object.
     *
     * @param ctx - an expression identifier context.
     * @return the expression tier.
     */
    @Override
    public Object visitExpression_identifier(JavaParser.Expression_identifierContext ctx) {
        visitChildren(ctx);
        Tier res = Tier.None;
        if (ctx.expression().getText().equals("this")) {
            res = getActualEnvironment();
        } else {
            String type = getType(ctx.expression());
            int no = getBlockNumber(ctx);
            no = tos.getClassBlockNumber(no);
            if (tos.getClassName(no).equals(type)) {
                res = getTier(ctx.expression());
            }
        }
        putTier(ctx, res, getActualEnvironment());
        return res;
    }

    /**
     * Handles method calls
     *
     * @param ctx - an expression call context.
     * @return the expression tier.
     */
    @Override
    public Object visitExpression_call(JavaParser.Expression_callContext ctx) {
        visitChildren(ctx);
        Tier res = Tier.None;
        Tier env = Tier.T0;
        ArrayList<String> param = new ArrayList<>();
        TierArray paramTiers = new TierArray(1);
        Mask mask = new Mask(1);
        String name = ctx.methodCall().IDENTIFIER().getText();
        String type;
        //Checks the types of the method call.
        if (ctx.methodCall().expressionList() != null) {
            List<JavaParser.ExpressionContext> list = ctx.methodCall().expressionList().expression();
            paramTiers = new TierArray(list.size() + 1);
            mask = new Mask(list.size() + 1);
            int i = 1;
            for (JavaParser.ExpressionContext e : list) {
                String t = getType(e);
                if (t.equals("int") || t.equals("boolean") || t.equals("null")) {
                    mask.set(i, true);
                }
                param.add(t);
                paramTiers.set(i, getTier(e));
                env = Tier.max(env, getEnvironmentTier(e));
                i++;
            }
        } else if (name.equals("clone")) {
            putTier(ctx, Tier.T0, Tier.T0);
            return Tier.T0;
        }
        //Checks the received type.
        String receiverName;
        if (ctx.expression() != null) {
            type = getType(ctx.expression());
            paramTiers.set(0, getTier(ctx.expression()));
            env = getEnvironmentTier(ctx.expression());
            receiverName = ctx.getText();
            receiverName = receiverName.replace("this.", "");
        } else {
            int no = getBlockNumber(ctx);
            type = tos.getClassName(no);
            paramTiers.set(0, getActualEnvironment());
            receiverName = "this";
        }
        //Builds the method namespace.
        String namespace = TOS.buildMethodNamespace(param, type, name);
        //Detects recursive calls: forces the environment tier to be T1 and increments the counter for recursive calls.
        if (containsRecursiveNamespace(namespace)) {
            incrementRecursiveCallsCount(receiverName);
            env = Tier.T1;
        } else {
            for (String current : getRecursiveNamespaces()) {
                String cn = current;
                while (!cn.contains("<root>::Object::") && !cn.equals(namespace)) {
                    cn = tos.getParentNamespace(cn);
                }
                if (cn.equals(namespace)) {
                    incrementRecursiveCallsCount(receiverName);
                    env = Tier.T1;
                } else {
                    cn = namespace;
                    while (!cn.contains("<root>::Object::") && !cn.equals(current)) {
                        cn = tos.getParentNamespace(cn);
                    }
                    if (cn.equals(current)) {
                        incrementRecursiveCallsCount(receiverName);
                        env = Tier.T1;
                    }
                }
            }
        }
        //Finds the namespace in the typing environment te and returns 
        //a compatible FOTier with the input tiers provided in the parameters.
        FOTier output = te.findMethod(namespace, paramTiers.getArray());
        if (output == null || output.getOut() == Tier.None) {
            output = te.findCompatibleMethod(namespace, mask, paramTiers.getArray());
            if (output == null) {
                res = Tier.None;
            }
        }
        if (output != null) {
            res = output.getOut();
            env = output.getEnv();
        }
        putTier(ctx, res, env);
        return res;
    }
}
