/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors.base;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.tos.id.VariableID;
import complexityparser.types.tieredtypes.VariableTieredType;
import complexityparser.types.Tier;
import org.antlr.v4.runtime.CommonTokenStream;
import java.util.HashSet;
import java.util.List;

/**
 * Base visitor extending BaseExpressionVisitor to handle statements.
 */
public abstract class BaseStatementVisitor extends BaseExpressionVisitor {

    public BaseStatementVisitor(TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        super(tosBuilder, typingListener, stream);
    }

    /**
     * Handles the typing of blocks.
     *
     * @param ctx - a block context.
     * @return the block tier.
     */
    @Override
    public Object visitBlock(JavaParser.BlockContext ctx) {
        visitChildren(ctx);
        List<JavaParser.BlockStatementContext> list = ctx.blockStatement();
        Tier res = Tier.T0;
        for (JavaParser.BlockStatementContext c : list) {
            Tier tmp = getTier(c);
            res = Tier.max(res, tmp);
        }
        putTier(ctx, res, getActualEnvironment());
        return res;
    }

    /**
     * Copies the tier type of a block statement context to it's associated
     * block parse tree.
     *
     * @param ctx - a block statement context.
     * @return - the copied tier.
     */
    @Override
    public Object visitStatement_block(JavaParser.Statement_blockContext ctx) {
        visitChildren(ctx);
        return copyTier(ctx.block(), ctx);
    }

    /**
     * Handles modifying the Tier type of variables listed in a declass block
     *
     * @param ctx - a block statement declassification context.
     * @return the default tier 0.
     */
    @Override
    public Object visitStatement_declass(JavaParser.Statement_declassContext ctx) {
        if (ctx.declassUnit() != null) {
            for (int i = 0; i < ctx.declassUnit().size(); i++) {
                String name = ctx.declassUnit(i).IDENTIFIER().getText();
                Tier tier = null;
                int block = getBlockNumber(ctx);
                VariableID id = new VariableID(name);
                VariableTieredType t = (VariableTieredType) tos.find(id, block);
                if (ctx.declassUnit(i).TIER() != null) {
                    String tmp = ctx.declassUnit(i).TIER().getText();
                    tmp = tmp.substring(1, tmp.length() - 1);
                    tier = Tier.fromString(tmp);
                } else {
                    if (t != null) {
                        tier = t.getTier();
                        tier = tier.next();
                    }
                }

                if (t != null && t.getTier() != Tier.None) {
                    t.setTier(tier);
                }
            }
        }
        putTier(ctx, Tier.T0, Tier.T0);
        return Tier.T0;
    }

    /**
     * Handles typing for a 'if' statement.
     *
     * @param ctx - a if statement context.
     * @return the tier of the 'if' statement.
     */
    @Override
    public Object visitStatement_if(JavaParser.Statement_ifContext ctx) {
        visit(ctx.parExpression());
        setLiteralDefaultTier(getTier(ctx.parExpression().expression()));
        //visitChildren(ctx);
        //Detects recursive calls in each branch and only keep the maximum value.
        //Does the same for while counts.
        int recursiveCalls = getRecursiveCallsCount();
        int recursiveCallsLocalMax = 0;
        int whileCount = getWhileCount();
        int whileCountLocalMax = 0;
        setRecursiveCallsCount(0);
        setWhileCount(0);
        HashSet<String> recursiveCallsReceivers = getRecursiveCallsReceivers();
        HashSet<String> recursiveCallsReceiversFinal = new HashSet<>(recursiveCallsReceivers.size());
        setRecursiveCallsReceivers((HashSet<String>) recursiveCallsReceivers.clone());
        for (int i = 0; i < ctx.statement().size(); i++) {
            visit(ctx.statement(i));
            recursiveCallsLocalMax = Math.max(recursiveCallsLocalMax, getRecursiveCallsCount());
            recursiveCallsReceiversFinal.addAll(getRecursiveCallsReceivers());
            setRecursiveCallsCount(0);
            setRecursiveCallsReceivers((HashSet<String>) recursiveCallsReceivers.clone());
            whileCountLocalMax = Math.max(whileCountLocalMax, getWhileCount());
            setWhileCount(0);
        }
        setRecursiveCallsCount(recursiveCalls + recursiveCallsLocalMax);
        setWhileCount(whileCount + whileCountLocalMax);
        recursiveCallsReceiversFinal.addAll(recursiveCallsReceivers);
        setRecursiveCallsReceivers(recursiveCallsReceiversFinal);
        resetPrimitiveDefaultTier();
        Tier parExpr = getTier(ctx.parExpression().expression());
        Tier env = getEnvironmentTier(ctx.parExpression().expression());
        Tier res = parExpr;
        //Types the if statement.
        if (parExpr != Tier.max(parExpr, env)) {
            res = Tier.None;
        }
        List<JavaParser.StatementContext> list = ctx.statement();
        Tier[] c = new Tier[list.size()];
        for (int i = 0; i < c.length; i++) {
            c[i] = getTier(list.get(i));
        }
        Tier max = parExpr;
        for (Tier t : c) {
            max = Tier.max(max, t);
        }
        if (parExpr != max) {
            res = Tier.None;
        }
        putTier(ctx, res, res);
        return res;
    }

    /**
     * Handles typing of a 'while' statement.
     *
     * @param ctx - a while statement context.
     * @return the while statement tier.
     */
    @Override
    public Object visitStatement_while(JavaParser.Statement_whileContext ctx) {
        visitChildren(ctx);
        JavaParser.ParExpressionContext par = ctx.parExpression();
        Tier parTier = getTier(par.expression());
        Tier env = getEnvironmentTier(par.expression());
        incrementWhileCount();
        //If the while statement contains recursive calls, the wile statement tier and environment tier are both NONE.
        Tier res;
        if (getRecursiveCallsCount() != 0) {
            res = Tier.None;
            env = Tier.None;
        }
        if (parTier == Tier.T1 && Tier.max(env, Tier.T1) == Tier.T1) {
            res = Tier.T1;
            if (getTier(ctx.statement()) == Tier.None || getTier(ctx.statement()) == null) {
                res = Tier.None;
            }
        } else {
            res = Tier.None;
        }
        putTier(ctx, res, Tier.T1);
        return res;
    }

    /**
     * Copies the tier of expressions to their associated statement parse tree.
     *
     * @param ctx - an expression statement context.
     * @return the max of the expression tier and the environment tier.
     */
    @Override
    public Object visitStatement_expression(JavaParser.Statement_expressionContext ctx) {
        visitChildren(ctx);
        Tier res = Tier.max(getTier(ctx.statementExpression), getEnvironmentTier(ctx.statementExpression));
        Tier env = Tier.T0;
        putTier(ctx, res, env);
        return res;
    }

    /**
     * Handles break statements (using tier 1).
     *
     * @param ctx - a beack statement context.
     * @return the tier 1 by default.
     */
    @Override
    public Object visitStatement_break(JavaParser.Statement_breakContext ctx) {
        visitChildren(ctx);

        putTier(ctx, Tier.T1, Tier.T0);
        return Tier.T1;
    }

    /**
     * Verifies if a catch clause is valid. Because literals are typed with tier
     * 1, an assignment will type 1 and thus not be a valid catch clause.
     *
     * @param ctx - a catch clause context.
     * @return the tier None (if the block has tier distinct from 0).
     */
    @Override
    public Object visitCatchClause(JavaParser.CatchClauseContext ctx) {
        setLiteralDefaultTier(Tier.T0);
        visitChildren(ctx);
        resetPrimitiveDefaultTier();
        Tier res = getTier(ctx.block());
        if (res != Tier.T0) {
            res = Tier.None;
        }
        putTier(ctx, res, Tier.T0);
        return res;
    }

    /**
     * Verifies if a finally clause is valid.
     *
     * @param ctx - a finally block context.
     * @return the tier of the block.
     */
    @Override
    public Object visitFinallyBlock(JavaParser.FinallyBlockContext ctx) {
        visitChildren(ctx);
        Tier res = getTier(ctx.block());
        putTier(ctx, res, Tier.T0);
        return res;
    }

    /**
     * Handles try {} catch {} statements.
     *
     * @param ctx - a try statement context.
     * @return the tier of the block.
     */
    @Override
    public Object visitStatement_try(JavaParser.Statement_tryContext ctx) {
        visitChildren(ctx);
        Tier res = Tier.T1;
        if (getTier(ctx.block()) == Tier.None) {
            res = Tier.None;
        }
        if (ctx.catchClause().size() != 0 && res != Tier.None) {
            for (JavaParser.CatchClauseContext c : ctx.catchClause()) {
                if (getTier(c) != Tier.T0) {
                    res = Tier.None;
                }
            }
        }
        if (ctx.finallyBlock() != null && res != Tier.None) {
            if (getTier(ctx.finallyBlock()) == Tier.None) {
                res = Tier.None;
            }
        }

        if (ctx.finallyBlock() == null && ctx.catchClause().size() == 0) {
            res = Tier.None;
        }
        putTier(ctx, res, Tier.T0);
        return res;
    }

    /**
     * Verifies throw statements.
     *
     * @param ctx - a throw statement context.
     * @return the tier of the block.
     */
    @Override
    public Object visitStatement_throw(JavaParser.Statement_throwContext ctx) {
        visitChildren(ctx);
        Tier res = Tier.max(Tier.T1, getTier(ctx.expression()));
        putTier(ctx, res, Tier.T0);
        return res;
    }
}
