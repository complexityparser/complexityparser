/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors.base;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.tos.id.VariableID;
import complexityparser.types.FOTier;
import complexityparser.types.Tier;
import complexityparser.types.tieredtypes.TieredType;
import complexityparser.types.tieredtypes.VariableTieredType;
import lib.Logger;
import lib.Mask;
import org.antlr.v4.runtime.CommonTokenStream;
import java.util.List;

/**
 * A visitor extending BaseVisitor in order to deals with expressions.
 */
public abstract class BaseExpressionVisitor extends BaseVisitor {

    public BaseExpressionVisitor(TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        super(tosBuilder, typingListener, stream);
    }

    /**
     * Copies the tier (and copies also the environment tier) of a primary
     * context to its associated primary expression parse tree.
     *
     * @param ctx - a primary context.
     * @return the copied tier.
     */
    @Override
    public Object visitExpression_primary(JavaParser.Expression_primaryContext ctx) {
        visit(ctx.primary());
        return copyTier(ctx.primary(), ctx);
    }

    /**
     * Copies the tier (and copies also the environment tier) of a primary
     * expression context to its associated primary expression parse tree.
     *
     * @param ctx - a primary expression context.
     * @return the copied tier.
     */
    @Override
    public Object visitPrimary_expression(JavaParser.Primary_expressionContext ctx) {
        visitChildren(ctx);
        return copyTier(ctx.expression(), ctx);
    }

    /**
     * Associates a tier and an environment tier (0 by default) to the current
     * expression parse tree.
     *
     * @param ctx - a primary this context.
     * @return the default tier T0.
     */
    @Override
    public Object visitPrimary_this(JavaParser.Primary_thisContext ctx) {
        visitChildren(ctx);

        putTier(ctx, getActualEnvironment(), Tier.T0);
        return Tier.T0;
    }

    /**
     * Associates a (default) tier and an environment tier (T0 by default) to a
     * literal.
     *
     * @param ctx - a primary literal context.
     * @return the default tier.
     */
    @Override
    public Object visitPrimary_literal(JavaParser.Primary_literalContext ctx) {
        visitChildren(ctx);
        putTier(ctx, getLiteralDefaultTier(), Tier.T0);
        return getLiteralDefaultTier();
    }

    /**
     * Associates a tier and an environment tier (T0 by default) to a variable. A
     * seach for the variable identifier is performed in the tos.
     *
     * @param ctx - a primary identifier context.
     * @return the tier of the variable.
     */
    @Override
    public Object visitPrimary_identifier(JavaParser.Primary_identifierContext ctx) {
        visitChildren(ctx);
        Tier res = Tier.None;
        int block = getBlockNumber(ctx);
        TieredType t = tos.find(new VariableID(ctx.IDENTIFIER().getText()), block);
        if (t != null) {
            if (tos.isBlockInClass(block, t.getNoBlock())) {
                res = getActualEnvironment();
            } else {
                res = t.getTier();
            }
        } else {
            Logger.errPrintln((new VariableID(ctx.IDENTIFIER().getText())) + " -> null");
        }
        putTier(ctx, res, Tier.T0);
        return res;
    }

    /**
     * Associates a tier to a 'new e' expression.
     *
     * @param ctx - a new expression context.
     * @return the tier T0 (None if some error occurs).
     */
    @Override
    public Object visitExpression_new(JavaParser.Expression_newContext ctx) {
        visitChildren(ctx);
        Tier env = Tier.T0;
        Tier res = Tier.T0;
        /*
        On a constructor call every parameter needs to be either of tier T0 or T0 compatible.
        Only primitive types are compatible.
         */
        if (ctx.creator().classCreatorRest().arguments().expressionList() != null) {
            for (JavaParser.ExpressionContext c : ctx.creator().classCreatorRest().arguments().expressionList().expression()) {
                env = Tier.max(env, getEnvironmentTier(c));
                String type = getType(c);
                if (c.getText().equals("null")) {
                    type = "null";
                }
                if (!(type.equals("int") || type.equals("boolean") || type.equals("null")) && getTier(c) != Tier.T0) {
                    res = Tier.None;
                }
            }
        }
        putTier(ctx, res, env);
        return res;
    }

    /**
     * Handles the typing for operations on expressions and sets the environment
     * tier to the max of the operand environment tiers.
     *
     * @param ctx - an operation context.
     * @return the output tier.
     */
    @Override
    public Object visitExpression_operation(JavaParser.Expression_operationContext ctx) {
        visitChildren(ctx);
        List<JavaParser.ExpressionContext> list = ctx.expression();
        String op = ctx.bop.getText();
        Tier res;
        Tier env = Tier.T0;
        Tier[] in = new Tier[list.size()];
        for (int i = 0; i < list.size(); i++) {
            in[i] = getTier(list.get(i));
            env = Tier.max(env, getEnvironmentTier(list.get(i)));
        }
        res = te.findRes(op, in).getOut();
        putTier(ctx, res, env);
        return res;
    }

    /**
     * Handles the typing for assignments.
     *
     * @param ctx - an expression assignment contex.
     * @return the tier of the assignment.
     */
    @Override
    public Object visitExpression_assignment(JavaParser.Expression_assignmentContext ctx) {
        visitChildren(ctx);
        int no = getBlockNumber(ctx);
        no = tos.getClassBlockNumber(no);
        List<JavaParser.ExpressionContext> list = ctx.expression();
        Tier res = Tier.None;
        String type = getType(list.get(0));
        Tier left = getTier(list.get(0));
        Tier right = getTier(list.get(1));
        String recName = list.get(0).getText();
        if (recName.contains("this.")) {
            recName = recName.replace("this.", "");
        }
        VariableID id = new VariableID(recName);
        VariableTieredType t = (VariableTieredType) tos.find(id, no);
        if (t != null && tos.isBlockInClass(t.getNoBlock(), tos.getClassBlockNumber(t.getNoBlock()))) {
            left = getActualEnvironment();
            res = getEnvironmentTier(list.get(1));
            if (type.equals("int") || type.equals("boolean")) {
                right = Tier.T0;
                if (Tier.min(left, right) != left) {
                    res = Tier.None;
                } else {
                }
            } else {
                if (left != Tier.T0 || right != Tier.T0) {
                    res = Tier.None;
                }
            }
        } else {
            res = Tier.max(right, getEnvironmentTier(list.get(1)));
            if (type.equals("int") || type.equals("boolean")) {
                if (left != Tier.min(left, right)) {
                    res = Tier.None;
                }
            } else {
                if (left != right) {
                    res = Tier.None;
                }
            }
        }
        putTier(ctx, res, Tier.T0);
        return res;
    }

    /**
     * Handles unary operators and methods.
     *
     * @param ctx - a unary expression context.
     * @return the output tier.
     */
    @Override
    public Object visitExpression_unary(JavaParser.Expression_unaryContext ctx) {
        visitChildren(ctx);
        Tier res = getTier(ctx.expression());
        Tier env = getEnvironmentTier(ctx.expression());
        String op = ctx.prefix.getText();
        String type = getType(ctx.expression());
        FOTier out = te.findRes(op, res);
        if (out == null || out.getOut() == Tier.None) {
            if (type.equals("int") || type.equals("boolean")) {
                Mask m = new Mask(1);
                m.set(0, true);
                out = te.findCompatibleIn(op, m, res);
            }
        }
        if (out != null) {
            res = out.getOut();
        } else {
            res = Tier.None;
        }
        putTier(ctx, res, env);
        return res;
    }
}
