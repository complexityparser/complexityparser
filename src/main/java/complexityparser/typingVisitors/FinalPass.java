/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.types.FOTierList;
import complexityparser.types.FOTier;
import complexityparser.types.Tier;
import complexityparser.typingVisitors.base.ObjectVisitor;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 * Performs a final pass on the tree, checks the constructors and generates a
 * string output for the method declarations (the first and second passes do not
 * generate any string output).
 */
public class FinalPass extends ObjectVisitor {

    public FinalPass(SecondPass sp, TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        super(tosBuilder, typingListener, stream);
        setResult(sp.getResult());
    }

    /**
     * The visit() method should not be called on the final pass. The ANTLR
     * visit(ParseTree) method should be called instead.
     */
    @Override
    public void visit() {
        throw new UnsupportedOperationException();
    }

    /**
     * Handles the type analysis of constructors.
     *
     * @param ctx - a constructor declaration context.
     * @return the tier of the constructor body.
     */
    @Override
    public Object visitConstructorDeclaration(JavaParser.ConstructorDeclarationContext ctx) {
        Tier oldEnv = getActualEnvironment();
        setActualEnvironment(Tier.T0);
        visitChildren(ctx);
        Tier res = getTier(ctx.block());
        if (res != Tier.T0) {
            res = Tier.None;
        }
        putTier(ctx, res, Tier.T0);
        setActualEnvironment(oldEnv);
        return res;
    }

    /**
     * Handles the final pass on method declarations. It consists in adding the
     * methods tier type to the hashmap, modifying the final result (if the
     * method does not type), and generating the string output (this feature is
     * handled by the putTier(ParseTree, Tier, EnvironmentTier) method).
     *
     * @param ctx - a method declaration context.
     * @return the tier of the body.
     */
    @Override
    public Object visitMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        String namespace = tos.getMethodNamespace(ctx, getBlockNumber(ctx));
        Tier resEnv = Tier.None;
        Tier res = Tier.None;
        int nbParam = 0;
        if (ctx.formalParameters().formalParameterList() != null) {
            nbParam = ctx.formalParameters().formalParameterList().formalParameter().size();
        }
        FOTierList op = te.get(namespace);
        if (op.size() == 0) {
            putTier(ctx, Tier.None, Tier.None);
        } else {
            FOTier o = op.get(0);
            putTier(ctx, o.getOut(), o.getEnv());
        }
        return null;
    }
}
