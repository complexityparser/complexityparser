/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors;

import complexityparser.types.Tier;
import complexityparser.types.FOTier;
import complexityparser.types.FOTierList;
import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.typingVisitors.base.ObjectVisitor;
import lib.graph.Graph;
import org.antlr.v4.runtime.CommonTokenStream;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Handles the second pass type analysis.
 */
public class SecondPass extends ObjectVisitor {

    public SecondPass(FirstPass fp, Graph g, TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        super(g, tosBuilder, typingListener, stream);
        setResult(fp.getResult());
    }

    /**
     * As long as the isOperatorModified returns true, it means that methods
     * FOTiers have been modified. Consequenlty, all the methods that
     * (functionnaly) depend on the previous one may not be valid anymore and
     * thus they must be analysed again. The second pass is iterated until a
     * fixpoint has been reached. The worst case will be None if no
     * other possibility can be found.
     */
    public void visit() {
        setOperatorModified(true);
        while (isOperatorModified()) {
            setOperatorModified(false);
            super.visit();
        }
    }

    /**
     * Handles method declarations by taking the FOTiers contained in the typing
     * environment hashmap and verifying that those FOTiers are correct and
     * compatible. Removes every FOTier that is not compatible. If none are
     * compatible, the FOTier output tier (and also environment tier) is set to None.
     *
     * @param ctx - a method declaration context.
     * @return the tier of the method body.
     */
    @Override
    public Object visitMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        String namespace = tos.getMethodNamespace(ctx, getBlockNumber(ctx));
        Tier resEnv = Tier.None;
        Tier res = Tier.None;
        int nbParam = 0;
        if (ctx.formalParameters().formalParameterList() != null) {
            nbParam = ctx.formalParameters().formalParameterList().formalParameter().size();
        }
        FOTierList op = te.get(namespace);
        setOverrideResult(false);
        int noBloc = getBlockNumber(ctx.methodBody().block());
        int i = 0;
        while (i < op.size()) {
            //This is performed as, by safety condition, while statements will not type when getRecursiveCallsCount() != 0
            setRecursiveCallsCount(0);
            setRecursiveCallsReceivers(new HashSet<>());
            setWhileCount(0);
            //Set the parameters types in the TOS.
            FOTier input = op.get(i);
            setActualEnvironment(input.getIn()[0]);
            if (nbParam != 0) {
                setParamTiers(ctx.formalParameters(), input.getIn(), noBloc);
            }
            //Visits the method body and checks that the given input is valid:
            //i.e. the body of the method types and that the return type is valid.
            visitMethodBody(ctx.methodBody());
            Tier tmp = null;
            Tier env = null;
            if (ctx.methodBody().block().expression() != null) {
                tmp = getTier(ctx.methodBody().block().expression());
            }
            env = getTier(ctx.methodBody().block());
            if (tmp == null) {
                tmp = env;
            }
            if (tmp != Tier.None && env != Tier.None) {
                i++;
                res = tmp;
                resEnv = env;
            } else {
                op.remove(i);
                setOperatorModified(true);
            }
        }
        if (op.size() == 0) {
            resEnv = Tier.None;
            res = Tier.None;
        }
        setOverrideResult(true);
        return res;
    }
}
