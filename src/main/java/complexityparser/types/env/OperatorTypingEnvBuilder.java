/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types.env;

import org.antlr.v4.runtime.tree.TerminalNode;
import java.io.IOException;
import java.util.List;
import complexityparser.types.Tier;
import complexityparser.types.FOTier;
import complexityparser.types.FOTierList;

/**
 * This class is only used to load the configuration file operators.config
 * containing a description (FOTierList) of the operators admissible FOTiers.
 */
public class OperatorTypingEnvBuilder extends OperatorsBaseListener {

    private TypingEnv te = new TypingEnv();
    private FOTierList latest;

    public TypingEnv getOp() {
        return te;
    }

    /**
     * Creates a new FOTierList, updates the attribute latest, and adds the new
     * object with the operator (or method) name to the TypingEnv te.
     *
     * @param ctx - an operator context.
     */
    @Override
    public void enterOperator(OperatorsParser.OperatorContext ctx) {
        if (te == null) {
            return;
        }
        super.enterOperator(ctx);
        latest = new FOTierList(true);
        String opString = ctx.STRING_LITERAL().getText();
        opString = opString.substring(1, opString.length() - 1);
        te.add(opString, latest);
    }

    /**
     * Creates a new entry in the FOTierList latest.
     *
     * @param ctx - a unit context.
     */
    @Override
    public void enterUnit(OperatorsParser.UnitContext ctx) {
        if (te == null) {
            return;
        }
        super.enterUnit(ctx);
        List<TerminalNode> list = ctx.input().TIER();
        Tier[] in = new Tier[list.size()];
        Tier out = Tier.fromString(ctx.result.getText());
        for (int i = 0; i < list.size(); i++) {
            in[i] = Tier.fromString(list.get(i).getText());
        }
        try {
            latest.add(out, Tier.T0, in);
        } catch (IOException e) {
            e.printStackTrace();
            te = null;
        }
    }
}
