/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types.tieredtypes;

import complexityparser.tos.TOS;
import complexityparser.types.Tier;

/**
 * An abstract class to represent a tiered type.
 */
public abstract class TieredType {

    protected Tier tier;
    protected int noBlock;
    protected String type;

    public TieredType(Tier tier, String type) {
        this.tier = tier;
        this.type = type;
        noBlock = TOS.getInstance().getCurrentBlock();
    }

    /**
     *
     * @return the tier of this tiered type.
     */
    public Tier getTier() {
        return tier;
    }

    /**
     *
     * @return the block number in which the current tiered type can be found.
     */
    public int getNoBlock() {
        return noBlock;
    }

    public void setTier(Tier tier) {
        this.tier = tier;
    }

    @Override
    public String toString() {
        return "TieredType{"
                + " tier = " + tier
                + ", noBlock = " + noBlock
                + ", type = '" + type + '\''
                + " }";
    }

    public String getMother() {
        return "Object";
    }

    public String getType() {
        return type;
    }
}
