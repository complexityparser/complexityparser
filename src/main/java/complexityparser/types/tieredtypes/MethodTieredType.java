/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types.tieredtypes;

import complexityparser.types.Tier;
import complexityparser.analyse.antlr.JavaParser;
import complexityparser.tos.TOS;

public class MethodTieredType extends TieredType {

    private int methodBlock;
    private JavaParser.MethodDeclarationContext ctx;

    public MethodTieredType(Tier tier, String type, JavaParser.MethodDeclarationContext ctx) {
        super(tier, type);
        methodBlock = TOS.getInstance().getNextBlockNumber();
        this.ctx = ctx;
    }

    public MethodTieredType(Tier tier, String type) {
        super(tier, type);
        methodBlock = TOS.getInstance().getNextBlockNumber();
    }

    public JavaParser.MethodDeclarationContext getCtx() {
        return ctx;
    }

    public int getMethodBlock() {
        return methodBlock;
    }

    @Override
    public String toString() {
        return "MethodTieredType { "
                + "methodBlock = " + methodBlock
                + ", tier = " + tier
                + ", noBlock = " + noBlock
                + ", type = '" + type + '\''
                + " }";
    }
}
