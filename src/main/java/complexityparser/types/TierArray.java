/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types;

import java.util.Arrays;

/**
 * A simple array containing tiers.
 */
public class TierArray {

    private Tier[] array;

    public TierArray(int size) {
        array = new Tier[size];
        for (int i = 0; i < size; i++) {
            array[i] = Tier.T0;
        }
    }

    /**
     * Sets the value at index i to tier v in the array (no checks are done on
     * the index).
     *
     * @param i - an index.
     * @param v - a tier.
     */
    public void set(int i, Tier v) {
        array[i] = v;
    }

    /**
     *
     * @param i - an index.
     * @return the value at index i (no checks are done on the index).
     */
    public Tier get(int i) {
        return array[i];
    }

    /**
     *
     * @return a deep copy of the array.
     */
    public Tier[] getArray() {
        return array.clone();
    }

    /**
     *
     * @return the size of the array
     */
    public int size() {
        return array.length;
    }

    /**
     * Computes the bitwise negation.
     */
    public void increment() {
        for (int i = 0; i < size(); i++) {
            switch (get(i)) {
                case T0:
                    set(i, Tier.T1);
                    return;
                case T1:
                    set(i, Tier.T0);
                    break;
                default:
                    return;
            }
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
