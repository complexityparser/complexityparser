/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types;

import lib.Mask;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import complexityparser.types.env.TypingEnv;

/**
 * Represents the list of admissible first order tiers (FOTier) of an operator,
 * constructor, or method. The boolean primitiveReturnType is true if the
 * return type is a primitive type and false if it is a reference type.
 */
public class FOTierList extends ArrayList<FOTier> {

    private int arity;
    private boolean primitiveReturnType;

    /**
     * Creates a new FOTierList. Empty by default. The arity being fixed to -1
     * indicating that the FOTierList has not been set yet.
     */
    public FOTierList(boolean primitiveReturnType) {
        super();
        arity = -1;
        this.primitiveReturnType = primitiveReturnType;
    }

    /**
     *
     * @return true if the return type is a primitive type, false otherwise.
     */
    public boolean isPrimitiveReturnType() {
        return primitiveReturnType;
    }

    /**
     * Adds a FOTier to the current FOTierList and sets the arity (or checks for
     * a coherent arity).
     *
     * @param out - the output tier .
     * @param env - the environment tier.
     * @param in - the input tiers.
     * @throws IOException
     */
    public void add(Tier out, Tier env, Tier... in) throws IOException {
        if (arity == -1) {
            arity = in.length;
        }
        if (arity != in.length) {
            //If the cardinality of the input tiers given as paramater is distinct 
            //from the FOTierList arity then an exception is thrown.
            throw new IOException("FOTierList::addObserver input of wrong size expected " + arity + " got " + in.length);
        }
        add(new FOTier(out, env, in));
    }

    /**
     *
     * @param in - input tiers.
     * @return the (first) FOTier matching the given input tiers in and a default FOTier 
     * of output tier None, if no match is found.
     */
    public FOTier findRes(Tier... in) {
        for (FOTier i : this) {
            if (i.contains(in)) {
                return i;
            }
        }
        return new FOTier(Tier.None, Tier.None, new Tier[0]);
    }

    /**
     *
     * @param m - a mask.
     * @param in - input tiers.
     * @return a compatible FOTier marching the input tiers in and the mask,
     * a default FOTier of output tier None if no match is found.
     */
    public FOTier findCompatibleRes(Mask m, Tier... in) {
        for (FOTier i : this) {
            if (i.isCompatibleWith(m, in)) {
                //The array is deep copied to avoid any unwanted side effect
                //and to provide another possible returned value 
                //(the current input tiers instead of the parameter input tiers, both being compatible).
                System.arraycopy(i.getIn(), 0, in, 0, in.length);
                return i;
            }
        }
        return new FOTier(Tier.None, Tier.None, new Tier[0]);
    }

    /**
     *
     * @param m - a mask.
     * @param in - input tiers.
     * @return an array of FOTiers of the FOTierList compatible with the input
     * parameter.
     */
    public FOTier[] findAllCompatibles(Mask m, Tier... in) {
        ArrayList<FOTier> res = new ArrayList<>(this.size());
        for (FOTier i : this) {
            if (i.isCompatibleWith(m, in)) {
                res.add(i);
            }
        }
        return res.toArray(new FOTier[0]);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (FOTier i : this) {
            str.append("\t");
            str.append(i);
            str.append(";\n");
        }
        return str.toString();
    }
}
