/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package lib.graph;

import org.antlr.v4.runtime.tree.ParseTree;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Represents a graph
 */
public class Graph {

    private ArrayList<Node> nodes;

    public Graph() {
        nodes = new ArrayList<>();
    }

    /**
     * Creates a node with a given name and a given parse tree.
     *
     * @param name - the name.
     * @param ctx - the parse tree.
     * @return the new node index.
     */
    public int createNode(String name, ParseTree ctx) {
        int i = nodes.size();
        nodes.add(new Node(this, name, ctx));
        return i;
    }

    /**
     * Creates a node from a given node.
     *
     * @param n - a node.
     * @return the new node number.
     */
    public int createNode(Node n) {
        int i = nodes.size();
        nodes.add(new Node(this, n));
        return i;
    }

    /**
     *
     * @param name - the name of a node.
     * @return the index of the node of the given name, -1 if not found
     */
    public int find(String name) {
        for (int i = 0; i < nodes.size(); i++) {
            if (name.equals(nodes.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param name - the name of a node.
     * @return the node of the given name, null if not found
     */
    public Node get(String name) {
        for (int i = 0; i < nodes.size(); i++) {
            if (name.equals(nodes.get(i).getName())) {
                return nodes.get(i);
            }
        }
        return null;
    }

    /**
     * Adds an arrow between the nodes of names src and dst. Create the nodes if
     * they are not present.
     *
     * @param src - the name of a node.
     * @param dst - the name of a node.
     */
    public void addConnection(String src, String dst) {
        int srci = find(src);
        int dsti = find(dst);
        if (srci == -1) {
            srci = createNode(src, null);
        }
        if (dsti == -1) {
            dsti = createNode(dst, null);
        }
        nodes.get(srci).add(dsti);
    }

    /**
     *
     * @param no - the index of a node.
     * @return the name associated with the given index.
     */
    public String getName(int no) {
        return nodes.get(no).getName();
    }

    /**
     *
     * @param name - the name of a node.
     * @return the node index associated with the given name.
     */
    public int getNo(String name) {
        for (int i = 0; i < size(); i++) {
            if (getName(i).equals(name)) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param no - a node number.
     * @return an iterator to walk through all the arrows of the node indexed by
     * no.
     */
    public Iterator<Integer> next(int no) {
        return nodes.get(no).iterator();
    }

    /**
     *
     * @return the number of arrows (nodes in the attribute nodes).
     */
    public int size() {
        return nodes.size();
    }

    /**
     *
     * @return a string representation of the graph using the DOT language
     */
    public String toDot() {
        StringBuilder str = new StringBuilder();
        str.append("digraph gr {\n");
        for (Node n : nodes) {
            for (int i : n) {
                str.append("\t\"");
                str.append(n.getName());
                str.append("\" -> \"");
                str.append(nodes.get(i).getName());
                str.append("\";\n");
            }
        }
        str.append("}");
        return str.toString();
    }

    /**
     *
     * @return a list containing the node names in suffix order (the DFS exit
     * order).
     */
    private ArrayList<String> dfsSuffix() {
        int s = 0;
        Stack<Pair> stack = new Stack<>();
        stack.ensureCapacity(nodes.size());
        boolean[] visited = new boolean[nodes.size()];
        ArrayList<String> suffix = new ArrayList<>(nodes.size());
        stack.push(new Pair(s, next(s)));
        visited[s] = true;
        while (suffix.size() != size()) {
            while (!stack.empty()) {
                Pair tmp = stack.peek();
                if (tmp.hasNext()) {
                    int v = tmp.next();
                    if (!visited[v]) {
                        visited[v] = true;
                        stack.push(new Pair(v, next(v)));
                    }
                } else {
                    suffix.add(getName(tmp.getFirst()));
                    stack.pop();
                }
            }
            for (s = 0; s < nodes.size() && visited[s]; s++);
            if (s < nodes.size()) {
                stack.push(new Pair(s, next(s)));
                visited[s] = true;
            }
        }
        return suffix;
    }

    /**
     * Executes a DFS on the graph using the order given in parameter and
     * generates an arraylist containing the strongly connected components.
     *
     * @param order - the reversed DFS-exit-order names of nodes.
     * @return the strongly connected components of the graph.
     */
    private ArrayList<ArrayList<Node>> dfs(ArrayList<String> order) {
        Stack<Pair> stack = new Stack<>();
        stack.ensureCapacity(nodes.size());
        boolean[] visited = new boolean[nodes.size()];
        ArrayList<ArrayList<Node>> components = new ArrayList<>();
        for (Node n : nodes) {
            n.sort(order);
        }
        for (String str : order) {
            int i = getNo(str);
            components.add(new ArrayList<>());
            //If the node has already been visited nothing is done.
            if (!visited[getNo(str)]) {
                //Else it is added to the stack and marked as visited.
                stack.push(new Pair(i, nodes.get(i).iterator()));
                visited[i] = true;
                //It is added to the current strongly connected components.
                components.get(components.size() - 1).add(nodes.get(i));
                /*
                Every node that is accessible from the current one
                that has not already been visited is also added
                to the strongly connected components.
                 */
                while (!stack.empty()) {
                    Pair tmp = stack.peek();
                    if (tmp.hasNext()) {
                        int v = tmp.next();
                        if (!visited[v]) {
                            components.get(components.size() - 1).add(nodes.get(v));
                            visited[v] = true;
                            stack.push(new Pair(v, nodes.get(v).iterator()));
                        }
                    } else {
                        //If all the connections from this node have been explored it is removed from the stack.
                        stack.pop();
                    }
                }
            }
        }
        //Empty components are removed.
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i).size() == 0) {
                components.remove(i);
                i--;
            }
        }
        return components;
    }

    /**
     *
     * @return the strongly connected components.
     */
    public ArrayList<ArrayList<Node>> getComponents() {
        ArrayList<String> suffix = dfsSuffix();
        Collections.reverse(suffix);
        ArrayList<ArrayList<Node>> components = mirror().dfs(suffix);
        return components;
    }

    /**
     *
     * @return a mirrored deep copy of the current graph.
     */
    public Graph mirror() {
        Graph g = new Graph();
        for (Node n : nodes) {
            g.createNode(n);
        }
        for (Node n : nodes) {
            for (int i : n) {
                g.addConnection(nodes.get(i).getName(), n.getName());
            }
        }
        return g;
    }

    /**
     * Creates a file with the given name containing the result of the toDot()
     * function.
     *
     * @param name - the file name.
     */
    public void toFile(String name) {
        try {
            FileWriter fw = new FileWriter(name);
            fw.append(toDot());
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return a string representation of the strongly connected components.
     */
    public String getComponentsString() {
        StringBuilder str = new StringBuilder();
        ArrayList<ArrayList<Node>> components = getComponents();
        for (ArrayList<Node> c : components) {
            for (Node n : c) {
                str.append(n + ", ");
            }
            str.append("\n--------------\n");
        }
        return str.toString();
    }
}
