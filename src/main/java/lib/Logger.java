/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package lib;

import java.io.PrintStream;

/**
 * Simple logger class if used to print messages to stdout, stdout can be easily
 * redirected to anything else.
 */
public class Logger {

    private static PrintStream out = System.out;
    private static PrintStream err = System.err;

    /**
     * set the standard output stream
     *
     * @param out
     */
    public static void setOut(PrintStream out) {
        Logger.out = out;
    }

    /**
     * set the standard error stream
     *
     * @param err
     */
    public static void setErr(PrintStream err) {
        Logger.err = err;
    }

    /**
     * print to the output stream
     *
     * @param str
     */
    public static void println(String str) {
        out.println(str);
    }

    /**
     * print to the error stream
     *
     * @param str
     */
    public static void errPrintln(String str) {
        err.println(str);
    }
}
